Emilio J. Gallego Arias
Mines ParisTech, PSL Research University

Experiments in Certified Digital Audio Processing
-------------------------------------------------

We will present recent developments in Wagner, a functional
programming language geared towards real-time digital audio
processing with formal, machine-checkable semantics.

The current system has three components:

1.- A Coq library with some facts about the Discrete Fourier Transform
    and unitary transforms. The formalization is constructive and
    heavily relies on the Mathematical Components library.

2.- A stream-oriented lambda calculus allowing the access to the
    previous values of variables. This language admits a coeffect
    based type-system for history tracking, and enables easy coding of
    filters and digital waveguide oscillators.

    We embed the language in Coq using step-indexing, crucially
    allowing a lightweight mechanization style as we can reuse most of
    Mathcomp's linear algebra libraries.

    We showcase the verification of our examples and the use of
    state-space representations in this framework.

3.- A call-by-value abstract machine provides the efficient execution
    model. Programs and buffers are typed for this machine using ideas
    from the focusing literature distinguishing "positive" and
    "negative" types. We finally embed the machine in Coq using the
    standard monadic CBV translation.

